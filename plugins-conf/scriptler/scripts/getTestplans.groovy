import jenkins.model.*
import groovy.io.FileType

def list = []
last_used = new File("/home/jenkins/logs/"+test_name+"/last_used_testplan")
if (last_used.exists()) {
  list += last_used.getText().replaceAll("\n","")
}
  

def dir = new File("/home/jenkins/overlays/testplans/")

deftp = new File("/home/jenkins/overlays/testplans/testplan_default.json")
if (deftp.exists()) {
  list += "testplan_default"
}

dir.eachFileRecurse (FileType.FILES) { file ->
  String[] pstr = file.getName().split("\\.")
  if (pstr.length == 2 && pstr[1].equals("json")) {
      list += pstr[0]
  }
}

list = list.unique()

return list
