# Copyright (c) 2014 Cogent Embedded, Inc.

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

# DESCRIPTION
# This script contains common utility functions


# prepend running python with ORIG_PATH if it exist
function run_python() {
    if [ ! -z $ORIG_PATH ]
    then
        echo "running python with PATH=$ORIG_PATH"
        PATH=$ORIG_PATH python "$@"
    else
        python "$@"
    fi
}

function abort_job {
# $1 - Abort reason string

  set +x
  echo -e "\n*** ABORTED ***\n"
  [ -n "$1" ] && echo -e "Fuego error reason: $1\n"

  wget -qO- ${BUILD_URL}/stop > /dev/null
  while true; do sleep 5; done
}

# check is variable is set and fail if otherwise
function assert_define () {
    varname=$1
    if [ -z "${!varname}" ]
    then
        abort_job "$1 is not defined. Make sure you use correct overlay with required specs for this test/benchmark"
    fi
}

assert_define "FUEGO_ENGINE_PATH"
export FUEGO_ENGINE_PATH=$FUEGO_ENGINE_PATH
export FUEGO_PARSER_PATH=$FUEGO_SCRIPTS_PATH/parser

TESTDIR="${JOB_NAME}"
TEST_HOME="$FUEGO_TESTS_PATH/${JOB_NAME}"
TRIPLET="${JOB_NAME}-$PLATFORM"

assert_define "FUEGO_ENGINE_PATH"
assert_define "FUEGO_SCRIPTS_PATH"
assert_define "FUEGO_LOGS_PATH"
assert_define "FUEGO_TESTS_PATH"
