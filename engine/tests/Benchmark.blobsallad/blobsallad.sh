tarball=blobsallad-src.tar.bz2

function test_build {
    patch -p0 -N -s < $TEST_HOME/blobsallad.Makefile.patch
    patch -p0 -N -s < $TEST_HOME/blobsallad.auto.patch
    patch -p0 -N -s < $TEST_HOME/bs_main.c.patch

    make CC="$CC" AR="$AR" RANLIB="$RANLIB" CXX="$CXX" CPP="$CPP" CXXCPP="$CXXCPP" SDKROOT="$SDKROOT"  && touch test_suite_ready || build_error "error while building test"
}

function test_deploy {
	put blobsallad  $FUEGO_HOME/fuego.$TESTDIR/
	put -r maps  $FUEGO_HOME/fuego.$TESTDIR/ 
}

function test_run {
	report "cd $FUEGO_HOME/fuego.$TESTDIR; export DISPLAY=:0; xrandr | awk '/\*/ {split(\$1, a, \"x\"); exit(system(\"./blobsallad \" a[1]  a[2]))}'"  
}

. $FUEGO_SCRIPTS_PATH/benchmark.sh
