#!/bin/bash

tarball=hello-test-1.1.tgz

function test_build {
	make && touch test_suite_ready || build_error "error while building test"
}

function test_deploy {
	put hello  $FUEGO_HOME/fuego.$TESTDIR/
}

function test_run {
#    assert_define FUNCTIONAL_HELLO_WORLD_ARG
    report "cd $FUEGO_HOME/fuego.$TESTDIR; ./hello $FUNCTIONAL_HELLO_WORLD_ARG"  
}

function test_processing {
    log_compare "$TESTDIR" "1" "SUCCESS" "p"          
}

. $FUEGO_SCRIPTS_PATH/functional.sh
