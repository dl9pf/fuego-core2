source $FUEGO_TESTS_PATH/OpenSSL/openssl.sh

function test_run {
	report "cd $FUEGO_HOME/fuego.$TESTDIR; bash run-tests.sh"  
}

function test_processing {
	P_CRIT="passed|ok"
	N_CRIT="skip"

	log_compare "$TESTDIR" "177" "${P_CRIT}" "p"
	log_compare "$TESTDIR" "86" "${N_CRIT}" "n"
}

test_run
get_testlog $TESTDIR
test_processing

