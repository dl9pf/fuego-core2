tarball=osdl-aim-7.0.1.13.tar.gz

function test_build {
        ./bootstrap
        PKG_CONFIG_PATH=${SDKROOT}/usr/lib/pkgconfig PKG_CONFIG_ALLOW_SYSTEM_LIBS=1 PKG_CONFIG_SYSROOT_DIR=${SDKROOT} ./configure --host=$HOST --build=`uname -m`-linux-gnu LDFLAGS=-L${SDKROOT}/usr/lib CPPFLAGS=-I${SDKROOT}/usr/include  CFLAGS=-I${SDKROOT}/usr/include LIBS=-laio --prefix=$FUEGO_HOME/$TESTDIR --datarootdir=$FUEGO_HOME/$TESTDIR
        make && touch test_suite_ready || build_error "error while building test"
}

function test_deploy {
	put src/reaim  $FUEGO_HOME/fuego.$TESTDIR/
	put -r data  $FUEGO_HOME/fuego.$TESTDIR/
	put -r scripts  $FUEGO_HOME/fuego.$TESTDIR/
}

function test_run {
	report "cd $FUEGO_HOME/fuego.$TESTDIR; mkdir /tmp/diskdir; ./reaim -c ./data/reaim.config -f ./data/workfile.short"  
	report_append "cd $FUEGO_HOME/fuego.$TESTDIR; ./reaim -c ./data/reaim.config -f ./data/workfile.all_utime"  
}

. $FUEGO_SCRIPTS_PATH/benchmark.sh
