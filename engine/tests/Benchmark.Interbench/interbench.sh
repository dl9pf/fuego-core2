tarball=interbench-0.31.tar.bz2

function test_build {
    patch -p0 < $TEST_HOME/interbench.c.patch
    make CC="$CC" AR="$AR" RANLIB="$RANLIB" CXX="$CXX" CPP="$CPP" CXXCPP="$CXXCPP" && touch test_suite_ready || build_error "error while building test"
}

function test_deploy {
	put interbench  $FUEGO_HOME/fuego.$TESTDIR/
}

function test_run {
	report "cd $FUEGO_HOME/fuego.$TESTDIR; ./interbench -L 1 || ./interbench -L 1"  
}

. $FUEGO_SCRIPTS_PATH/benchmark.sh
