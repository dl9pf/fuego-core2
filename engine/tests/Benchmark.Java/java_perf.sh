tarball=java_perf.tar

function test_build {
    touch test_suite_ready
}

function test_deploy {
    put *.jar  $FUEGO_HOME/fuego.$TESTDIR/
}

function test_run {
    report "cd $FUEGO_HOME/fuego.$TESTDIR; java -cp scimark2lib.jar jnt.scimark2.commandline"  
    report_append "cd $FUEGO_HOME/fuego.$TESTDIR; java -jar dacapo-9.12-bach.jar avrora" $FUEGO_HOME/ 
    report_append "cd $FUEGO_HOME/fuego.$TESTDIR; java -jar dacapo-9.12-bach.jar jython"  
    report_append "cd $FUEGO_HOME/fuego.$TESTDIR; java -jar dacapo-9.12-bach.jar luindex"  
    report_append "cd $FUEGO_HOME/fuego.$TESTDIR; java -jar dacapo-9.12-bach.jar lusearch"  
    #report_append "cd $FUEGO_HOME/fuego.$TESTDIR; java -jar dacapo-9.12-bach.jar tomcat"  
    report_append "cd $FUEGO_HOME/fuego.$TESTDIR; java -jar dacapo-9.12-bach.jar xalan"  
}

. $FUEGO_SCRIPTS_PATH/benchmark.sh
