source $FUEGO_SCRIPTS_PATH/overlays.sh
set_overlay_vars

source $FUEGO_SCRIPTS_PATH/reports.sh
source $FUEGO_SCRIPTS_PATH/functions.sh

source $TEST_HOME/../LTP/ltp.sh

function test_run {
    report "cd $FUEGO_HOME/fuego.$TESTDIR; mkdir tmp; ./runltp -d tmp -f fs_fuego"  
}

test_run
get_testlog $TESTDIR
