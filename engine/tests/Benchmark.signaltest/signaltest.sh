tarball=signaltest.tar.gz

function test_build {
  make CC="$CC" LD="$LD" LDFLAGS="$LDFLAGS" CFLAGS="$CFLAGS" && touch test_suite_ready || build_error "error while building test"
}

function test_deploy {
	put signaltest  $FUEGO_HOME/fuego.$TESTDIR/
}

function test_run {
	assert_define BENCHMARK_SIGNALTEST_LOOPS

	report "cd $FUEGO_HOME/fuego.$TESTDIR; ./signaltest  -l $BENCHMARK_SIGNALTEST_LOOPS -q"  
}

. $FUEGO_SCRIPTS_PATH/benchmark.sh
