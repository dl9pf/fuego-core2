tarball=lmbench3.tar.gz

function test_build {
   mkdir -p SCCS
   touch SCCS/s.ChangeSet
   cd scripts
   patch -p0 < $TEST_HOME/lmbench3.config-run.patch
   patch -p0 < $TEST_HOME/lmbench.patch
   patch -p0 < $TEST_HOME/lmbench3.mem64.patch
   cd ../src
   patch -p0 < $TEST_HOME/bench.h.patch
   cd ..
   CFLAGS+=" -g -O"
   make OS="$PREFIX" CC="$CC" AR="$AR" RANLIB="$RANLIB" CXX="$CXX" CPP="$CPP" CXXCPP="$CXXCPP" CFLAGS="$CFLAGS" && touch test_suite_ready || build_error "error while building test"
}

function test_deploy {
   put -r *  $FUEGO_HOME/fuego.$TESTDIR/
}

function test_run {
   LMBENCH_OS=`ls ./bin`
   safe_cmd "rm -rf $FUEGO_HOME/fuego.$TESTDIR/results"
   safe_cmd "cd $FUEGO_HOME/fuego.$TESTDIR/scripts; OS=$LMBENCH_OS ./config-run"
   safe_cmd "cd $FUEGO_HOME/fuego.$TESTDIR/scripts; OS=$LMBENCH_OS ./results"
   report "cd $FUEGO_HOME/fuego.$TESTDIR/scripts; ./getsummary ../results/$LMBENCH_OS/*.0"
}

. $FUEGO_SCRIPTS_PATH/benchmark.sh
