tarball=reboot

function test_build {
	true
}

function test_deploy {
	put $TEST_HOME/$tarball  $FUEGO_HOME/fuego.$TESTDIR/
}

function test_run {
	target_reboot
	report "cd $FUEGO_HOME/fuego.$TESTDIR; ./reboot"  
}

. $FUEGO_SCRIPTS_PATH/benchmark.sh
