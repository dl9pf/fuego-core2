tarball=nbench-byte-2.2.3.tar.gz

function test_build {
    patch -N -s -p0 < $TEST_HOME/nbench.Makefile.patch
    rm -f pointer.h && touch pointer.h
    CFLAGS+=" -s -static -Wall -O3"
    make CFLAGS="${CFLAGS}" CC="$CC" AR="$AR" RANLIB="$RANLIB" CXX="$CXX" CPP="$CPP" CXXCPP="$CXXCPP" && touch test_suite_ready || build_error "error while building test"
}

function test_deploy {
	put hardware nbench sysinfo.sh *.DAT  $FUEGO_HOME/fuego.$TESTDIR/
}

function test_run {
	report  "cd $FUEGO_HOME/fuego.$TESTDIR; ./nbench"  
}

. $FUEGO_SCRIPTS_PATH/benchmark.sh
