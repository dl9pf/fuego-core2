tarball=fontconfig-2.6.0.tar.gz

function test_build {
	sed -i -e "s/FCLIST=\.\.\/fc\-list\/fc\-list/FCLIST=\/usr\/bin\/fc\-list/g" -e "s/FCCACHE=\.\.\/fc\-cache\/fc\-cache/FCCACHE=\/usr\/bin\/fc\-cache/g" test/run-test.sh
}

function test_deploy {
	put -r test/*  $FUEGO_HOME/fuego.$TESTDIR/
}

function test_run {
	report "cd $FUEGO_HOME/fuego.$TESTDIR; if ''bash run-test.sh''; then echo 'TEST PASS'; else echo 'TEST FAIL'; fi"  
}

function test_processing {
	log_compare "$TESTDIR" "1" "TEST PASS" "p"
	log_compare "$TESTDIR" "0" "TEST FAIL" "n"
}

. $FUEGO_SCRIPTS_PATH/functional.sh
