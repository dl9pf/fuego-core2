OF.NAME="base-distrib"
OF.DESCRIPTION="Base distribution commands"

function ov_get_firmware() {
    FW=`cmd "uname -r | xargs echo "` || abort_job "Unable to get firmware version"
}

function ov_rootfs_reboot() {
    cmd "/sbin/reboot &"
}

function ov_rootfs_state() {
    cmd "echo; uptime; echo; free; echo; df -h; echo; mount; echo; ps |grep -Fv '  ['; echo; cat /proc/interrupts; echo" || abort_job "Error while  ROOTFS_STATE command execution on target"
}

function ov_logger() {
    cmd "logger $@" || abort_job "Could not execute ROOTFS_LOGGER command"
}

function ov_rootfs_sync() {
    cmd "sync" || abort_job "Unable to flush buffers on target" 
}

function ov_rootfs_drop_caches() {
    cmd "echo 3 > /proc/sys/vm/drop_caches" || abort_job "Unable to drop filesystem caches"
}

function ov_rootfs_oom() {
    cmd "echo 1000 > /proc/\$\$/oom_score_adj" || abort_job "Unable to set OOM score adj."
}

# Kill any stale processes if requested to do so.
# First, issue normal kill, and finally, if stale process was found, force its termination with signal 9.
function ov_rootfs_kill() {
    [ -n "$2" ] && cmd "pkill $2 && sleep 2 && pkill -9 $2; true"
    [ -n "$3" ] && cmd "pkill $3 && sleep 2 && pkill -9 $3; true"
    [ -n "$4" ] && cmd "pkill $4 && sleep 2 && pkill -9 $4; true"
    true
}


# We create /tmp/${2} dir in any case to capture target logs and prevent
# log dump to $FUEGO_HOME dir.
function ov_rootfs_logread() {
    cmd "mkdir -p /tmp/fuego.${1} && cd /tmp/fuego.${1} && /sbin/logread > ${NODE_NAME}.${BUILD_ID}.${BUILD_NUMBER}.$2" || abort_job "Error while ROOTFS_LOGREAD command execution on target"
}

