{
    "testName": "Functional.bc",
    "fail_case": [
        {
            "fail_regexp": "some test regexp",
            "fail_message": "some test message"
        },
        {
            "fail_regexp": "Bug",
            "fail_message": "Bug or Oops detected in system log",
            "use_syslog": "1"
        }
        ],
    "specs": 
    [
        {
            "name":"bc-mult",
            "EXPR":"2*2",
            "RESULT": "4"
        },
        {
            "name":"bc-add",
            "EXPR":"3+3",
            "RESULT":"6"
        },
        {
            "name":"default",
            "EXPR":"3+3",
            "RESULT":"6"
        }
    ]
}

